import java.util.Base64
import java.util.UUID
import javax.crypto.Cipher
import javax.crypto.Mac
import javax.crypto.spec.IvParameterSpec
import javax.crypto.spec.SecretKeySpec
import kotlin.experimental.inv
import kotlin.experimental.xor
import kotlin.io.path.Path
import kotlin.io.path.exists
import kotlin.io.path.readText
import kotlin.io.path.writeText
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.StateFlow
import kotlinx.serialization.KSerializer
import kotlinx.serialization.Serializable
import kotlinx.serialization.builtins.MapSerializer
import kotlinx.serialization.descriptors.PrimitiveKind
import kotlinx.serialization.descriptors.PrimitiveSerialDescriptor
import kotlinx.serialization.descriptors.SerialDescriptor
import kotlinx.serialization.encoding.Decoder
import kotlinx.serialization.encoding.Encoder
import kotlinx.serialization.json.Json
import net.mm2d.upnp.Service

private val applicationIdRegex = Regex("<X_ApplicationId>([a-zA-Z0-9+/=]+)</X_ApplicationId>")
private val keywordRegex = Regex("<X_Keyword>([a-zA-Z0-9+/=]+)</X_Keyword>")
private val sessionIdRegex = Regex("<X_SessionId>([0-9]+)</X_SessionId>")
private val json = Json { }
private val keyFile = Path("keys.json")
private val keyFileSerializer = MapSerializer(UUIDSerializer, ApplicationKeys.serializer())

suspend fun processPanasonic(service: Service, online: StateFlow<Boolean>) {
    val keyList = if (keyFile.exists()) {
        println("Loading keys...")
        val contents = keyFile.readText()
        json.decodeFromString(keyFileSerializer, contents).toMutableMap()
    } else {
        mutableMapOf()
    }
    val udn = UUID.fromString(service.device.udn.removePrefix("uuid:"))
    val appKeys = if (keyList.containsKey(udn)) {
        keyList[udn]!!
    } else {
        println("Executing first time setup...")
        val keys = initialSetup(service) ?: return
        keyList[udn] = keys
        println("Saving new key...")
        val content = json.encodeToString(keyFileSerializer, keyList)
        keyFile.writeText(content)
        keys
    }

    println("Fetching session id...")
    val sessionId = fetchSessionId(service, appKeys)
    println("Ready!")

    var sequence = 1

    val encryptedCommandAction = service.findAction("X_EncryptedCommand")
    checkNotNull(encryptedCommandAction) { "Action X_EncryptedCommand not found" }
    while (true) {
        delay(20000)
        val request = """
            <X_SessionId>$sessionId</X_SessionId>
            <X_SequenceNumber>${(sequence++).toString().padStart(8, '0')}</X_SequenceNumber>
            <X_OriginalCommand>
              <u:X_SendKey xmlns:u="urn:panasonic-com:service:p00NetworkControl:1">
                <X_KeyEvent>NRC_POWER-ONOFF</X_KeyEvent>
              </u:X_SendKey>
            </X_OriginalCommand>
        """.trimIndent()
        val encInfo = appKeys.keys.encryptSoapPayload(request)
        if (online.value) {
            println("Turning TV off...")
        } else {
            println("Turning TV on...")
        }
        val result = encryptedCommandAction.invokeAsync(
            mapOf(
                "X_ApplicationId" to appKeys.applicationId, "X_EncInfo" to encInfo
            )
        )
        val response = appKeys.keys.decryptSoapPayload(result["X_EncResult"]!!)
        println(response)
    }
}

private suspend fun fetchSessionId(service: Service, appKeys: ApplicationKeys): String {
    val encInfo = appKeys.keys.encryptSoapPayload("<X_ApplicationId>${appKeys.applicationId}</X_ApplicationId>")
    val sessionIdRes = service.findAction("X_GetEncryptSessionId")!!
        .invokeAsync(mapOf("X_ApplicationId" to appKeys.applicationId, "X_EncInfo" to encInfo))["X_EncResult"]!!
    return decryptSessionId(appKeys.keys, sessionIdRes)
}

internal fun decryptSessionId(keys: EncryptionKeys, sessionIdRes: String): String {
    val sessionIdText = keys.decryptSoapPayload(sessionIdRes)
    val sessionIdMatch = sessionIdRegex.find(sessionIdText)
    checkNotNull(sessionIdMatch) { "Can't find session id in response: $sessionIdText" }
    return sessionIdMatch.groupValues[1]
}

private suspend fun initialSetup(service: Service): ApplicationKeys? {
    val displayPinCode = service.findAction("X_DisplayPinCode")
    checkNotNull(displayPinCode) { "Action X_DisplayPinCode not found" }
    val requestAuth = service.findAction("X_RequestAuth")
    checkNotNull(requestAuth) { "Action X_RequestAuth not found" }

    val displayPinRes = displayPinCode.invokeAsync(mapOf("X_DeviceName" to "Kotlin Test"), returnErrorResponse = true)
    if ("faultcode" in displayPinRes) {
        if (displayPinRes["UPnPError/errorCode"] == "412") {
            println("TV \"${service.device.friendlyName}\" is offline, turn on to authenticate!")
            return null
        }
        println("Error: $displayPinRes")
    }
    val challengeKey = displayPinRes["X_ChallengeKey"]
    val keySet = deriveKeySetFromChallengeKey(Base64.getDecoder().decode(challengeKey))
    val pin = readPin()

    val authInfo = keySet.encryptSoapPayload("<X_PinCode>$pin</X_PinCode>")
    val authRes = requestAuth.invokeAsync(mapOf("X_AuthInfo" to authInfo))["X_AuthResult"]!!
    return applicationKeysFromAuthResult(keySet, authRes)
}

internal fun applicationKeysFromAuthResult(keySet: EncryptionKeys, authRes: String): ApplicationKeys {
    val keys = keySet.decryptSoapPayload(authRes)
    val applicationIdMatch = applicationIdRegex.find(keys)
    checkNotNull(applicationIdMatch) { "Did not find the app id in the auth info response: \"$keys\"" }
    val applicationId = applicationIdMatch.groupValues[1]
    val keywordMatch = keywordRegex.find(keys)
    checkNotNull(keywordMatch) { "Did not find the keyword in the auth info response: \"$keys\"" }
    val keyword = Base64.getDecoder().decode(keywordMatch.groupValues[1])

    val appKeys = deriveKeySetFromKeyword(keyword)
    return ApplicationKeys(applicationId, appKeys)
}

internal fun deriveKeySetFromKeyword(keyword: ByteArray): EncryptionKeys {
    val appKey = ByteArray(16)
    for (i in 0 until 16 step 4) {
        appKey[i] = keyword[i + 2]
        appKey[i + 1] = keyword[i + 3]
        appKey[i + 2] = keyword[i]
        appKey[i + 3] = keyword[i + 1]
    }
    val appHmacKey = keyword + keyword

    return EncryptionKeys(appKey, appHmacKey, keyword)
}

private fun readPin(): String {
    while (true) {
        print("\nPIN: ")
        val line = readLine() ?: throw IllegalStateException("Invalid input stream!")
        if (line.length != 4) {
            println("Pin has to be 4 numbers (length: ${line.length})!")
            continue
        }
        val num = line.toIntOrNull()
        if (num == null) {
            println("Pin has to be 4 numbers!")
            continue
        }
        return line
    }
}

private val hmacKeyMask = "15C95AC2B08AA7EB4E228F811E34D04FA54BA7DCAC9879FA8ACDA3FC244F3854".decodeHex()

internal fun deriveKeySetFromChallengeKey(challengeKey: ByteArray): EncryptionKeys {
    require(challengeKey.size == 16) { "Challenge key has to be 16 bytes" }
    val key = ByteArray(16)
    for (i in 0 until 16 step 4) {
        key[i] = challengeKey[i + 3].inv()
        key[i + 1] = challengeKey[i + 2].inv()
        key[i + 2] = challengeKey[i + 1].inv()
        key[i + 3] = challengeKey[i].inv()
    }

    val hmac = ByteArray(32)
    for (i in 0 until 32 step 4) {
        hmac[i] = hmacKeyMask[i] xor challengeKey[(i + 2) and 0xF]
        hmac[i + 1] = hmacKeyMask[i + 1] xor challengeKey[(i + 3) and 0xF]
        hmac[i + 2] = hmacKeyMask[i + 2] xor challengeKey[i and 0xF]
        hmac[i + 3] = hmacKeyMask[i + 3] xor challengeKey[(i + 1) and 0xF]
    }

    return EncryptionKeys(key, hmac, challengeKey)
}

private fun String.decodeHex(): ByteArray {
    require(length % 2 == 0) { "Must have an even length" }

    return chunked(2).map { it.toInt(16).toByte() }.toByteArray()
}

@Serializable
internal class EncryptionKeys(
    private val key: ByteArray, private val hmacKey: ByteArray, private val iv: ByteArray
) {
    fun encryptSoapPayload(data: String): String {
        val n = data.length
        val payload = StringBuilder(16 + n)
        payload.apply {
            append("000000000000")
            append(Char(n shr 24))
            append(Char((n shr 16) and 0xFF))
            append(Char((n shr 8) and 0xFF))
            append(Char(n and 0xFF))
            append(data)
        }

        val cipher = Cipher.getInstance("AES/CBC/PKCS5Padding")
        cipher.init(Cipher.ENCRYPT_MODE, SecretKeySpec(key, "AES"), IvParameterSpec(iv))
        val ciphertext = cipher.doFinal(payload.toString().toByteArray())
        val mac = Mac.getInstance("HmacSHA256")
        mac.init(SecretKeySpec(hmacKey, "HmacSHA256"))
        val sig = mac.doFinal(ciphertext)
        return Base64.getEncoder().encodeToString(ciphertext + sig)
    }

    fun decryptSoapPayload(data: String): String {
        val cipher = Cipher.getInstance("AES/CBC/NoPadding")
        cipher.init(Cipher.DECRYPT_MODE, SecretKeySpec(key, "AES"), IvParameterSpec(iv))
        val bytes = Base64.getDecoder().decode(data)
        val ciphertext = bytes.sliceArray(0..(bytes.size - 33))
        val testSig = bytes.sliceArray((bytes.size - 32) until bytes.size)
        val mac = Mac.getInstance("HmacSHA256")
        mac.init(SecretKeySpec(hmacKey, "HmacSHA256"))
        val sig = mac.doFinal(ciphertext)
        check(sig.contentEquals(testSig)) { "Computed signature doesn't match the signature" }
        val plaintext = cipher.doFinal(ciphertext).decodeToString()
        return "<${plaintext.substringAfter('<').substringBeforeLast('>')}>"
    }
}

@Serializable
internal class ApplicationKeys(val applicationId: String, val keys: EncryptionKeys)

private object UUIDSerializer : KSerializer<UUID> {
    override val descriptor: SerialDescriptor = PrimitiveSerialDescriptor("UUID", PrimitiveKind.STRING)

    override fun deserialize(decoder: Decoder): UUID = UUID.fromString(decoder.decodeString())

    override fun serialize(encoder: Encoder, value: UUID) = encoder.encodeString(value.toString())
}
